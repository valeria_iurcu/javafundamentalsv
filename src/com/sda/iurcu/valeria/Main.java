package com.sda.iurcu.valeria;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

       // printName();
        printTable();
        printTableWithStringFormat();
        printFormattedNumber();
        printNumberTriangle(5);
        System.out.println();
        printNumberInTriangle(readNumberFromConsole());

    }

    public static void printName(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdu numele de familie:");
        String name = scanner.nextLine();
        System.out.println("Introdu prenumele:");
        String surname = scanner.nextLine();

        System.out.printf("Numele de familie este %10s si prenumele este %-10s" + '.', name, surname );

    }

    public static void printTable(){

        System.out.printf("%s %21s \n", "Exam name", "Exam grade" );
        System.out.println("-------------------------------------------------");
        System.out.printf("%-24s %s \n", "Java", "A");
        System.out.printf("%-24s %s \n", "PHP", "B");
        System.out.printf("%-24s %s \n", "VB NET", "A");
        System.out.println("-------------------------------------------------");

    }

    private static void printTableWithStringFormat(){
        System.out.println(String.format("%-20s %s", "Exam Name", "Exam Grade"));
        System.out.println("-------------------------------------------");
        System.out.println(String.format("%-24s %s", "Java", "A"));
        System.out.println(String.format("%-24s %s", "Php", "B"));
        System.out.println(String.format("%-24s %s", "VB net", "A"));
        System.out.println("-------------------------------------------");
    }

    private static void printFormattedNumber(){
        System.out.printf("Numarul %-11.1f este formatat \n", 5.77);
    }

    public static void printNumberTriangle(int n){

        for (int i = 1; i <= n; i++){
            for (int j = 1; j<= i; j++){
            System.out.print(j + " ");
            }
            System.out.println("");
        }
    }
    public static int readNumberFromConsole(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdu un numar: ");
        return scanner.nextInt();
    }

    public static void printNumberInTriangle(int number){
        String text = "";
        for (int i = 1; i <= number; i++){
            text = text + i + " ";
            System.out.println(text);
        }
    }

}

