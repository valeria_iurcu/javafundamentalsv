package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

public class Task1 {

    public static void main(String[] args) {
       // System.out.println("Numarul introdus este: " + ScannerUtils.getNumberFromConsole());
        Task1 circlePer = new Task1();
        circlePer.calculateCirclePer();

    }

    public void calculateCirclePer(){
        System.out.println("Introdu diametrul cercului: ");
        float diameter = ScannerUtils.getFloatNumberFromConsole();
        System.out.println("Perimetrul cercului este: " + Math.PI * diameter);
    }



}
