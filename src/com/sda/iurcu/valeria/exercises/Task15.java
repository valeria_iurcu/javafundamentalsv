package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

public class Task15 {
    public static void main(String[] args) {
        Task15 task15 = new Task15();
        task15.printNumberThatOccurTwice();
    }
    public void printNumberThatOccurTwice(){
        System.out.println("Introdu 10 numere");
        int[] numbersFromUser = new int[10];
        for (int i = 0; i < numbersFromUser.length; i++) {
            numbersFromUser[i] = ScannerUtils.getNumberFromConsole();
        }
        for (int i = 0; i < numbersFromUser.length; i++) {
            if (numbersFromUser[i] == -1) {
                continue;
            }
            int duplicateNumber = -1;
            for (int j = i + 1; j < numbersFromUser.length; j++) {
                if (numbersFromUser[i] == numbersFromUser[j]) {
                    duplicateNumber = numbersFromUser[j];
                    numbersFromUser[j] = -1;
                }
            }
            if (duplicateNumber != -1){
                System.out.println(duplicateNumber);
            }
        }
    }
}
