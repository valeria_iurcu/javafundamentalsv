package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

import java.util.Scanner;

public class Task9 {

    public static void main(String[] args) {
        Task9 task9 = new Task9();
        System.out.println("Insert number of lines for drawing 'v'");
        int lines = ScannerUtils.getNumberFromConsole();
        System.out.println("Insert number of v's to be drawn");
        int wavesNumber = ScannerUtils.getNumberFromConsole();
        task9.drawWaves(lines, wavesNumber);

    }

    public void drawWaves(int lines, int wavesNumber){
        for (int i = 1; i <= lines; i++){
            for (int j = 1; j <= wavesNumber; j++) {
                int numberOfSpaceBetween = (lines * 2) - (i * 2);
                String star = "*";
                String space = "";
                String line = "%" + i + "s" + "%" + numberOfSpaceBetween + "s" + "%-" + i + "s";

                if (numberOfSpaceBetween == 0) {
                    line = "%" + i + "s" + "%s" + "%-" + i + "s";
                }
                System.out.printf(line, star, space, star);
            }
            System.out.println();
        }

    }

}
