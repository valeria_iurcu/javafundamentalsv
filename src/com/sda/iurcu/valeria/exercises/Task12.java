package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

public class Task12 {
    public static void main(String[] args) {
        Task12 percentage = new Task12();
        percentage.findOccurrenceOfSpaceCharacter();
    }
    public void findOccurrenceOfSpaceCharacter(){
        System.out.println("Introdu un text");
        String input = ScannerUtils.getScanner().nextLine();
        int totalLength = input.length();
        int numberOfSpaces = input.split(" ").length - 1;
        int precentage = numberOfSpaces * 100 / totalLength;
        System.out.println("Procentul de spatii este: " + precentage + "%");
    }
}
