package com.sda.iurcu.valeria.exercises;

public class Fibonacci {

    public static void main(String[] args) {

        Fibonacci fibonacci = new Fibonacci();
        System.out.println(fibonacci.calculateFibonacciNumber(1));

    }

    public int calculateFibonacciNumber(int index) {
        if (index == 0 || index == 1) {
            return 1;
        }
        int number1 = 1;
        int number2 = 1;
        int result = 0;

        for (int i = 2; i <= index; i++) {
            result = number1 + number2;
            number1 = number2;
            number2 = result;
        }
        return result;

    }

}
