package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

public class Task16 {
    public static void main(String[] args) {
        Task16 task16 = new Task16();
        System.out.println("Cel mai lung sir de numere: " + task16.longestConsecutive());
    }

    public int longestConsecutive() {
        System.out.println("Introdu 10 numere");
        int[] numbersFromUser = new int[10];
        for (int i = 0; i < numbersFromUser.length; i++) {
            numbersFromUser[i] = ScannerUtils.getNumberFromConsole();
        }
        int consecutiveNumbers = 1;
        int longestNumber = 1;
        for (int i = 0; i < numbersFromUser.length; i++) {
            if (numbersFromUser[i] < numbersFromUser[i + 1]) {
                consecutiveNumbers++;
                if (longestNumber < consecutiveNumbers) {
                    longestNumber = consecutiveNumbers;
                }
            } else {
                consecutiveNumbers = 1;
            }
            if (i == numbersFromUser.length - 2) {
                break;
            }
        }
        return longestNumber;
    }
}
