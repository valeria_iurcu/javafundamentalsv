package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Task17 {
    public static void main(String[] args) {
        Task17 task17 = new Task17();
        task17.calculateDaysBetween();
    }
    public void calculateDaysBetween(){
        System.out.println("Introdu data in format de: dd MM yyyy");
        String input = ScannerUtils.getScanner().nextLine();
        LocalDate inputDate = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd MM yyyy"));
        LocalDate localDate = LocalDate.now();
        Period period = Period.between(localDate, inputDate);
        System.out.println(period);
    }
}
