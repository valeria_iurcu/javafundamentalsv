package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

import java.util.Scanner;

public class Task11 {

    public static void main(String[] args) {
        Task11 task11 = new Task11();
        task11.findLongestText();
    }
    public void findLongestText(){
        Scanner scanner = ScannerUtils.getScanner();
        String input;
        String longestText = "";
        System.out.println("Introdu cate texte vrei, cand vrei sa te opresti scrie 'Enough!'");
        do {
            input = scanner.nextLine();
            if (input.length() > longestText.length() && !input.equals("Enough!")){
                longestText = input;
            }

        }while (!input.equals("Enough!"));

        if (longestText.length() > 0){
            System.out.println(longestText);
        }else {
            System.out.println("No text provided");
        }

    }
}
