package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

public class Task14 {
    public static void main(String[] args) {
        Task14 task14 = new Task14();
        task14.countLettersBetween();

    }
    public void countLettersBetween(){
        System.out.println("Introdu doua litere din alfabet");
        char firstLetter = ScannerUtils.getScanner().nextLine().charAt(0);
        char secondLetter = ScannerUtils.getScanner().nextLine().charAt(0);
        int numberOfCharacters = secondLetter - firstLetter - 1;
        System.out.println("Intre litera: " + firstLetter + " si litera: " + secondLetter + " sunt: " + numberOfCharacters);

    }

}
