package com.sda.iurcu.valeria.exercises;

import com.sda.iurcu.valeria.utils.ScannerUtils;

public class Task2 {
    public static void main(String[] args) {
      Task2 fizzBuzz = new Task2();
      fizzBuzz.fizzBuzz();
    }
    public void fizzBuzz(){
        System.out.println("Introdu un numar pozitiv: ");
        int number = ScannerUtils.getNumberFromConsole();

        for (int i = 1; i <= number; i++) {

            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println("Fizz Buzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 7 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }

    }

}
