package com.sda.iurcu.valeria.utils;

import java.util.Scanner;

public class ScannerUtils {

    private static Scanner scanner;

    public static int getNumberFromConsole(){
        //System.out.println("Introdu un numar");
        Scanner scanner = getScanner();
        int userNumberinput;
        try {
            userNumberinput = scanner.nextInt();
        }catch (Exception exception){
            System.out.println("Introdu un numar valid");
            scanner.nextLine();
            userNumberinput = getNumberFromConsole();
        }
        return userNumberinput;
    }

    public static Scanner getScanner() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        return scanner;
    }

    public static float getFloatNumberFromConsole() {
        Scanner scanner = getScanner();
        float userNumberinput;
        try {
            userNumberinput = scanner.nextInt();
        } catch (Exception exception) {
            System.out.println("Introdu un numar valid");
            scanner.nextLine();
            userNumberinput = getFloatNumberFromConsole();
        }
        return userNumberinput;
    }


}
